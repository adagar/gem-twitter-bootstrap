= gem "Twitter-Bootstrap "

This application is a sample application that uses the Twitter-Bootstrap gem.
This application is made using Rails 3. 

== Environment 
* ruby - 1.9.3
* rails - 3.1.1

== Features 
* It is a fully featured store. 

== List of commands used
  rails new store
  cd store
  rails g scaffold product name:string price:decimal --skip-stylesheets
  rake db:migrate
  rails s
  rm public/index.html
  * add the path to root
  * initialize git and push changes online
  * add the twitter-bootstrap-rails gem in the gem file
  bundle install
  rails g bootstrap:install
  rails s
  * the application already looks good at the moment
  * make changes to application.html.erb
  * reload the page.. rails s to see the changes
  rails s
  * visit the page http://twitter.github.com/bootstrap/components.html#navbar and select the code for the navigation bar. 
  * add the code in the application.html.erb file and you will get a navigation bar up top. 
  * had to change the padding at the top as -40px so that the gap at the top can be removed. 
  * works fine
  * added some code in the application.html.erb so that the application works fine with Internet Explorer as well
  * added some code in the application.html.erb so that the application works fine with Mobile Devices as well
  * add data to seed.rb
  rake db:seed
  rails g bootstrap:themed products -f
  rails s
  * add the code to provide flash messages for creating product etc
  * add validations for product model
  gem 'simple_form' # add this in the gem file
  rails g simple_form:install --bootstrap
  rails s
  * check how the application works
  

== Other projects

For more projects please visit:
https://github.com/abhishekdagarit
